//CALCULAR EL IVA

const product = {count: 3, price: 12.55, type: "ropa"};
getVat(product);

function getVat(product) {
    let total = product.count * product.price;
    let iva = 0.21;
    
    if (total > 0) {
        switch(product.type) {
            case "alimentacion":
                iva = 0.10;
                break;
            case "libro":
                iva = 0.04;
                break;
            default:
                iva = 0.21;        
        }
    
        let ivaTotal = total * iva;
        total += ivaTotal;
        console.log("Artículo: " + product.type);
        console.log("Cantidad: " + product.count);
        console.log("Precio: " + product.price + "€");
        console.log("IVA: " + iva*100 + "%");
        console.log("Precio con IVA: " + total.toFixed(2) + "€");
        
    } else {
        total = 0;
        console.log("Precio con IVA: " + total.toFixed(2) + "€");
    }
}


//SUELDO NETO
console.log("");
console.log("------------------------");
console.log("------------------------");
console.log("");

const empleado = {
    bruto: 14500,
    hijos: 2,
    pagas: 14
}

getNeto(empleado);

function getNeto(empleado) {
    let retencion = 0;
    sueldoBruto = empleado.bruto;

    if(sueldoBruto < 12000) {
        retencion = 0;
    } else if(sueldoBruto < 24000) {
        retencion = 8;
    } else if(sueldoBruto < 34000) {
        retencioon= 16;
    } else {
        rentecion = 30;
    }

    if (empleado.hijos > 0) {
        retencion -= 2;
    }

    let retencionTotal = sueldoBruto * (retencion/100);
    let sueldoNeto = sueldoBruto - retencionTotal;
    console.log("Sueldo bruto: " + sueldoBruto + "€");
    console.log("Hijos: " + empleado.hijos);
    console.log("Pagas: " + empleado.pagas);
    console.log("Retención: " + retencion + "%");
    console.log("Sueldo neto anual: " + sueldoNeto + "€");
    console.log("Sueldo neto mensual: " + (sueldoNeto/empleado.pagas).toFixed(2) + "€");
}